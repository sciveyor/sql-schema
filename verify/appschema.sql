-- Verify sciveyor:appschema on pg

BEGIN;

SELECT pg_catalog.has_schema_privilege('sciveyor', 'usage');

ROLLBACK;
