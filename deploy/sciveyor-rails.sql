-- Deploy sciveyor:sciveyor-rails to pg
-- requires: appschema

BEGIN;

SET client_min_messages = 'warning';

CREATE TABLE sciveyor.categories (
  id         BIGINT      PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  name       TEXT        NOT NULL,
  path       INTEGER[]   NOT NULL DEFAULT '{}',
  journals   TEXT[]      NOT NULL DEFAULT '{}'
);

CREATE INDEX index_categories_on_path
  ON sciveyor.categories
  USING gin (path);

CREATE TABLE sciveyor.datasets (
  id               BIGINT    PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  uuid             TEXT      NOT NULL,
  q                TEXT[]    NOT NULL DEFAULT '{}',
  "boolean"        TEXT      NOT NULL DEFAULT 'and',
  fq               TEXT[]    NOT NULL DEFAULT '{}',
  document_count   INTEGER   DEFAULT 0
);

CREATE INDEX index_datasets_on_uuid
  ON sciveyor.datasets (uuid);

CREATE TABLE sciveyor.datasets_tasks (
  dataset_id   BIGINT   NOT NULL,
  task_id      BIGINT   NOT NULL
);

CREATE INDEX index_datasets_tasks_on_dataset_id
  ON sciveyor.datasets_tasks (dataset_id);
CREATE INDEX index_datasets_tasks_on_task_id
  ON sciveyor.datasets_tasks (task_id);

CREATE TABLE sciveyor.users (
  id                       BIGINT      PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  email                    TEXT        NOT NULL DEFAULT '',
  name                     TEXT,
  language                 TEXT        NOT NULL DEFAULT 'en',
  timezone                 TEXT        NOT NULL DEFAULT 'America/New_York',
  workflow_active          BOOLEAN     NOT NULL DEFAULT false,
  workflow_class           TEXT,
  workflow_datasets        TEXT[]      NOT NULL DEFAULT '{}',
  export_requested_at      TIMESTAMP,
  export_filename          TEXT,
  export_content_type      TEXT,
  created_at               TIMESTAMP,
  updated_at               TIMESTAMP,
  encrypted_password       TEXT          NOT NULL DEFAULT '',
  reset_password_token     TEXT,
  reset_password_sent_at   TIMESTAMP,
  remember_created_at      TIMESTAMP,
  sign_in_count            INTEGER       NOT NULL DEFAULT 0,
  current_sign_in_at       TIMESTAMP,
  last_sign_in_at          TIMESTAMP,
  current_sign_in_ip       TEXT,
  last_sign_in_ip          TEXT
);

CREATE INDEX index_users_on_email
  ON sciveyor.users (email);
CREATE INDEX index_users_on_reset_password_token
  ON sciveyor.users (reset_password_token);

CREATE TABLE sciveyor.libraries (
  id        BIGINT   PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  name      TEXT     NOT NULL,
  url       TEXT     NOT NULL,
  user_id   BIGINT   NOT NULL REFERENCES sciveyor.users(id)
);

CREATE INDEX index_libraries_on_user_id
  ON sciveyor.libraries (user_id);

CREATE TABLE sciveyor.tasks (
  id            BIGINT      PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  name          TEXT,
  type          TEXT,
  user_id       BIGINT      NOT NULL REFERENCES sciveyor.users(id),
  finished      BOOLEAN     NOT NULL DEFAULT false,
  failed        BOOLEAN     NOT NULL DEFAULT false,
  created_at    TIMESTAMP,
  updated_at    TIMESTAMP,
  finished_at   TIMESTAMP,
  job_message   TEXT,
  job_params    TEXT
);

CREATE INDEX index_tasks_on_user_id
  ON sciveyor.tasks (user_id);

CREATE TABLE sciveyor.jobs (
  id           BIGINT      PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  task_id      BIGINT      NOT NULL REFERENCES sciveyor.tasks(id),
  created_at   TIMESTAMP
);

CREATE TABLE sciveyor.uploads (
  id                  BIGINT    PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  filename            TEXT      NOT NULL,
  content_type        TEXT,
  description         TEXT,
  short_description   TEXT,
  downloadable        BOOLEAN   NOT NULL DEFAULT false,
  task_id             BIGINT    NOT NULL REFERENCES sciveyor.tasks(id)
);

CREATE TABLE sciveyor.users_datasets (
  user_id        BIGINT NOT NULL,
  dataset_uuid   TEXT NOT NULL,
  name           TEXT NOT NULL
);

CREATE INDEX index_users_datasets_on_user_id
  ON sciveyor.users_datasets (user_id);
CREATE INDEX index_users_datasets_on_dataset_uuid
  ON sciveyor.users_datasets (dataset_uuid);

COMMIT;
