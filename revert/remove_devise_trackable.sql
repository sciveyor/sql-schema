-- Revert sciveyor:remove_devise_trackable from pg

BEGIN;

ALTER TABLE sciveyor.users
  ADD COLUMN sign_in_count INTEGER NOT NULL DEFAULT 0;
ALTER TABLE sciveyor.users
  ADD COLUMN current_sign_in_at TIMESTAMP;
ALTER TABLE sciveyor.users
  ADD COLUMN last_sign_in_at TIMESTAMP;
ALTER TABLE sciveyor.users
  ADD COLUMN current_sign_in_ip TEXT;
ALTER TABLE sciveyor.users
  ADD COLUMN last_sign_in_ip TEXT;

COMMIT;
