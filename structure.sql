--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: sciveyor; Type: SCHEMA; Schema: -; Owner: -
--

CREATE SCHEMA sciveyor;


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: categories; Type: TABLE; Schema: sciveyor; Owner: -
--

CREATE TABLE sciveyor.categories (
    id bigint NOT NULL,
    name text NOT NULL,
    path integer[] DEFAULT '{}'::integer[] NOT NULL,
    journals text[] DEFAULT '{}'::text[] NOT NULL
);


--
-- Name: categories_id_seq; Type: SEQUENCE; Schema: sciveyor; Owner: -
--

ALTER TABLE sciveyor.categories ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME sciveyor.categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: datasets; Type: TABLE; Schema: sciveyor; Owner: -
--

CREATE TABLE sciveyor.datasets (
    id bigint NOT NULL,
    uuid text NOT NULL,
    q text[] DEFAULT '{}'::text[] NOT NULL,
    "boolean" text DEFAULT 'and'::text NOT NULL,
    fq text[] DEFAULT '{}'::text[] NOT NULL,
    document_count integer DEFAULT 0
);


--
-- Name: datasets_id_seq; Type: SEQUENCE; Schema: sciveyor; Owner: -
--

ALTER TABLE sciveyor.datasets ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME sciveyor.datasets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: datasets_tasks; Type: TABLE; Schema: sciveyor; Owner: -
--

CREATE TABLE sciveyor.datasets_tasks (
    dataset_id bigint NOT NULL,
    task_id bigint NOT NULL
);


--
-- Name: jobs; Type: TABLE; Schema: sciveyor; Owner: -
--

CREATE TABLE sciveyor.jobs (
    id bigint NOT NULL,
    task_id bigint NOT NULL,
    created_at timestamp without time zone
);


--
-- Name: jobs_id_seq; Type: SEQUENCE; Schema: sciveyor; Owner: -
--

ALTER TABLE sciveyor.jobs ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME sciveyor.jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: libraries; Type: TABLE; Schema: sciveyor; Owner: -
--

CREATE TABLE sciveyor.libraries (
    id bigint NOT NULL,
    name text NOT NULL,
    url text NOT NULL,
    user_id bigint NOT NULL
);


--
-- Name: libraries_id_seq; Type: SEQUENCE; Schema: sciveyor; Owner: -
--

ALTER TABLE sciveyor.libraries ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME sciveyor.libraries_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: tasks; Type: TABLE; Schema: sciveyor; Owner: -
--

CREATE TABLE sciveyor.tasks (
    id bigint NOT NULL,
    name text,
    type text,
    user_id bigint NOT NULL,
    finished boolean DEFAULT false NOT NULL,
    failed boolean DEFAULT false NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    finished_at timestamp without time zone,
    job_message text,
    job_params text,
    attempts bigint DEFAULT 0 NOT NULL
);


--
-- Name: tasks_id_seq; Type: SEQUENCE; Schema: sciveyor; Owner: -
--

ALTER TABLE sciveyor.tasks ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME sciveyor.tasks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: uploads; Type: TABLE; Schema: sciveyor; Owner: -
--

CREATE TABLE sciveyor.uploads (
    id bigint NOT NULL,
    filename text NOT NULL,
    content_type text,
    description text,
    short_description text,
    downloadable boolean DEFAULT false NOT NULL,
    task_id bigint NOT NULL
);


--
-- Name: uploads_id_seq; Type: SEQUENCE; Schema: sciveyor; Owner: -
--

ALTER TABLE sciveyor.uploads ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME sciveyor.uploads_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: users; Type: TABLE; Schema: sciveyor; Owner: -
--

CREATE TABLE sciveyor.users (
    id bigint NOT NULL,
    email text DEFAULT ''::text NOT NULL,
    name text,
    language text DEFAULT 'en'::text NOT NULL,
    timezone text DEFAULT 'America/New_York'::text NOT NULL,
    workflow_active boolean DEFAULT false NOT NULL,
    workflow_class text,
    workflow_datasets text[] DEFAULT '{}'::text[] NOT NULL,
    export_requested_at timestamp without time zone,
    export_filename text,
    export_content_type text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    encrypted_password text DEFAULT ''::text NOT NULL,
    reset_password_token text,
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone
);


--
-- Name: users_datasets; Type: TABLE; Schema: sciveyor; Owner: -
--

CREATE TABLE sciveyor.users_datasets (
    user_id bigint NOT NULL,
    dataset_uuid text NOT NULL,
    name text NOT NULL
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: sciveyor; Owner: -
--

ALTER TABLE sciveyor.users ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME sciveyor.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- Name: categories categories_pkey; Type: CONSTRAINT; Schema: sciveyor; Owner: -
--

ALTER TABLE ONLY sciveyor.categories
    ADD CONSTRAINT categories_pkey PRIMARY KEY (id);


--
-- Name: datasets datasets_pkey; Type: CONSTRAINT; Schema: sciveyor; Owner: -
--

ALTER TABLE ONLY sciveyor.datasets
    ADD CONSTRAINT datasets_pkey PRIMARY KEY (id);


--
-- Name: jobs jobs_pkey; Type: CONSTRAINT; Schema: sciveyor; Owner: -
--

ALTER TABLE ONLY sciveyor.jobs
    ADD CONSTRAINT jobs_pkey PRIMARY KEY (id);


--
-- Name: libraries libraries_pkey; Type: CONSTRAINT; Schema: sciveyor; Owner: -
--

ALTER TABLE ONLY sciveyor.libraries
    ADD CONSTRAINT libraries_pkey PRIMARY KEY (id);


--
-- Name: tasks tasks_pkey; Type: CONSTRAINT; Schema: sciveyor; Owner: -
--

ALTER TABLE ONLY sciveyor.tasks
    ADD CONSTRAINT tasks_pkey PRIMARY KEY (id);


--
-- Name: uploads uploads_pkey; Type: CONSTRAINT; Schema: sciveyor; Owner: -
--

ALTER TABLE ONLY sciveyor.uploads
    ADD CONSTRAINT uploads_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: sciveyor; Owner: -
--

ALTER TABLE ONLY sciveyor.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: index_categories_on_path; Type: INDEX; Schema: sciveyor; Owner: -
--

CREATE INDEX index_categories_on_path ON sciveyor.categories USING gin (path);


--
-- Name: index_datasets_on_uuid; Type: INDEX; Schema: sciveyor; Owner: -
--

CREATE INDEX index_datasets_on_uuid ON sciveyor.datasets USING btree (uuid);


--
-- Name: index_datasets_tasks_on_dataset_id; Type: INDEX; Schema: sciveyor; Owner: -
--

CREATE INDEX index_datasets_tasks_on_dataset_id ON sciveyor.datasets_tasks USING btree (dataset_id);


--
-- Name: index_datasets_tasks_on_task_id; Type: INDEX; Schema: sciveyor; Owner: -
--

CREATE INDEX index_datasets_tasks_on_task_id ON sciveyor.datasets_tasks USING btree (task_id);


--
-- Name: index_libraries_on_user_id; Type: INDEX; Schema: sciveyor; Owner: -
--

CREATE INDEX index_libraries_on_user_id ON sciveyor.libraries USING btree (user_id);


--
-- Name: index_tasks_on_user_id; Type: INDEX; Schema: sciveyor; Owner: -
--

CREATE INDEX index_tasks_on_user_id ON sciveyor.tasks USING btree (user_id);


--
-- Name: index_users_datasets_on_dataset_uuid; Type: INDEX; Schema: sciveyor; Owner: -
--

CREATE INDEX index_users_datasets_on_dataset_uuid ON sciveyor.users_datasets USING btree (dataset_uuid);


--
-- Name: index_users_datasets_on_user_id; Type: INDEX; Schema: sciveyor; Owner: -
--

CREATE INDEX index_users_datasets_on_user_id ON sciveyor.users_datasets USING btree (user_id);


--
-- Name: index_users_on_email; Type: INDEX; Schema: sciveyor; Owner: -
--

CREATE INDEX index_users_on_email ON sciveyor.users USING btree (email);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: sciveyor; Owner: -
--

CREATE INDEX index_users_on_reset_password_token ON sciveyor.users USING btree (reset_password_token);


--
-- Name: jobs jobs_task_id_fkey; Type: FK CONSTRAINT; Schema: sciveyor; Owner: -
--

ALTER TABLE ONLY sciveyor.jobs
    ADD CONSTRAINT jobs_task_id_fkey FOREIGN KEY (task_id) REFERENCES sciveyor.tasks(id);


--
-- Name: libraries libraries_user_id_fkey; Type: FK CONSTRAINT; Schema: sciveyor; Owner: -
--

ALTER TABLE ONLY sciveyor.libraries
    ADD CONSTRAINT libraries_user_id_fkey FOREIGN KEY (user_id) REFERENCES sciveyor.users(id);


--
-- Name: tasks tasks_user_id_fkey; Type: FK CONSTRAINT; Schema: sciveyor; Owner: -
--

ALTER TABLE ONLY sciveyor.tasks
    ADD CONSTRAINT tasks_user_id_fkey FOREIGN KEY (user_id) REFERENCES sciveyor.users(id);


--
-- Name: uploads uploads_task_id_fkey; Type: FK CONSTRAINT; Schema: sciveyor; Owner: -
--

ALTER TABLE ONLY sciveyor.uploads
    ADD CONSTRAINT uploads_task_id_fkey FOREIGN KEY (task_id) REFERENCES sciveyor.tasks(id);


--
-- PostgreSQL database dump complete
--

