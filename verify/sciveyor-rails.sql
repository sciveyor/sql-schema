-- Verify sciveyor:sciveyor-rails on pg

BEGIN;

SELECT id, name, path, journals
  FROM sciveyor.categories
WHERE FALSE;

SELECT 1/COUNT(*) FROM pg_indexes
  WHERE schemaname = 'sciveyor' AND
        indexname = 'index_categories_on_path';

SELECT id, uuid, q, "boolean", fq, document_count
  FROM sciveyor.datasets
WHERE FALSE;

SELECT 1/COUNT(*) FROM pg_indexes
  WHERE schemaname = 'sciveyor' AND
        indexname = 'index_datasets_on_uuid';

SELECT dataset_id, task_id
  FROM sciveyor.datasets_tasks
WHERE FALSE;

SELECT 1/COUNT(*) FROM pg_indexes
  WHERE schemaname = 'sciveyor' AND
        indexname = 'index_datasets_tasks_on_dataset_id';
SELECT 1/COUNT(*) FROM pg_indexes
  WHERE schemaname = 'sciveyor' AND
        indexname = 'index_datasets_tasks_on_task_id';

SELECT id, email, name, language, timezone, workflow_active, workflow_class,
       workflow_datasets, export_requested_at, export_filename,
       export_content_type, created_at, updated_at, encrypted_password,
       reset_password_token, reset_password_sent_at, remember_created_at,
       sign_in_count, current_sign_in_at, last_sign_in_at,
       current_sign_in_ip, last_sign_in_ip
  FROM sciveyor.users
WHERE FALSE;

SELECT 1/COUNT(*) FROM pg_indexes
  WHERE schemaname = 'sciveyor' AND
        indexname = 'index_users_on_email';
SELECT 1/COUNT(*) FROM pg_indexes
  WHERE schemaname = 'sciveyor' AND
        indexname = 'index_users_on_reset_password_token';

SELECT id, name, url, user_id
  FROM sciveyor.libraries
WHERE FALSE;

SELECT 1/COUNT(*) FROM pg_indexes
  WHERE schemaname = 'sciveyor' AND
        indexname = 'index_libraries_on_user_id';

SELECT id, name, type, user_id, finished, failed, created_at, updated_at,
       finished_at, job_message, job_params
  FROM sciveyor.tasks
WHERE FALSE;

SELECT 1/COUNT(*) FROM pg_indexes
  WHERE schemaname = 'sciveyor' AND
        indexname = 'index_tasks_on_user_id';

SELECT id, task_id, created_at
  FROM sciveyor.jobs
WHERE FALSE;

SELECT id, filename, content_type, description, short_description,
       downloadable, task_id
  FROM sciveyor.uploads
WHERE FALSE;

SELECT user_id, dataset_uuid, name
  FROM sciveyor.users_datasets
WHERE FALSE;

SELECT 1/COUNT(*) FROM pg_indexes
  WHERE schemaname = 'sciveyor' AND
        indexname = 'index_users_datasets_on_user_id';
SELECT 1/COUNT(*) FROM pg_indexes
  WHERE schemaname = 'sciveyor' AND
        indexname = 'index_users_datasets_on_dataset_uuid';

ROLLBACK;
