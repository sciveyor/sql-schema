-- Verify sciveyor:remove_devise_trackable on pg

BEGIN;

DO $$
DECLARE
  count INTEGER;
BEGIN
  count := (SELECT COUNT(*) FROM information_schema.columns
    WHERE table_schema = 'sciveyor'
    AND table_name = 'users'
    AND column_name = 'sign_in_count');
  ASSERT(count = 0);
  
  count := (SELECT COUNT(*) FROM information_schema.columns
    WHERE table_schema = 'sciveyor'
    AND table_name = 'users'
    AND column_name = 'current_sign_in_at');
  ASSERT(count = 0);
  
  count := (SELECT COUNT(*) FROM information_schema.columns
    WHERE table_schema = 'sciveyor'
    AND table_name = 'users'
    AND column_name = 'last_sign_in_at');
  ASSERT(count = 0);
  
  count := (SELECT COUNT(*) FROM information_schema.columns
    WHERE table_schema = 'sciveyor'
    AND table_name = 'users'
    AND column_name = 'current_sign_in_ip');
  ASSERT(count = 0);
  
  count := (SELECT COUNT(*) FROM information_schema.columns
    WHERE table_schema = 'sciveyor'
    AND table_name = 'users'
    AND column_name = 'last_sign_in_ip');
  ASSERT(count = 0);
END $$;

ROLLBACK;
