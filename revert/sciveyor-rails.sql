-- Revert sciveyor:sciveyor-rails from pg

BEGIN;

DROP TABLE sciveyor.users_datasets;
DROP TABLE sciveyor.uploads;
DROP TABLE sciveyor.jobs;
DROP TABLE sciveyor.tasks;
DROP TABLE sciveyor.libraries;
DROP TABLE sciveyor.users;
DROP TABLE sciveyor.datasets_tasks;
DROP TABLE sciveyor.datasets;
DROP TABLE sciveyor.categories;

COMMIT;
