<p align="center">
<img src="https://docs.sciveyor.com/images/icon-orange.svg" width="20%"
height="auto" alt="Sciveyor Logo">
</p>

# Sciveyor SQL Schema

This repository contains a [Sqitch](http://sqitch.org/) project for building,
migrating, and verifying the PostgreSQL database schema for Sciveyor. This SQL
schema must be deployed to PostgreSQL 10 or higher, as we make extensive use of
Postgres `IDENTITY` columns.

You can look at a user-friendly representation of this schema by visiting our
[SQL database documentation.](https://docs.sciveyor.com/schemaspy/)

## Deploying and Migrating

Edit `sqitch.conf` to create a new target for your deployment database. The
easiest way to do this is by running:

```sh
sqitch target add flipr_test db:pg:flipr_test
```

If you want this target to become the default, edit `sqitch.conf` and set
`target = (your target)` in the `[engine "pg"]` section. This is not
recommended, however, as it will be too easy to accidentally clobber changes to
your data (the default target is here set to a test database).

You then deploy your changes by running:

```sh
sqitch deploy <target>
```

This will deploy and automatically verify the deployment, based upon the current
status of your database. To check the database's status, you can run, for
instance:

```sh
sqitch status <target>
```

## Full Schema

If you would like to read through the entire schema, as dumped from a current
PostgreSQL instance, you can consult the `structure.sql` file. Note that this
file is not read by Sqitch, and has nothing to do with the status or migrations
of your server.

## License

This database schema is released under the MIT license.
