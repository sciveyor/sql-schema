-- Deploy sciveyor:remove_devise_trackable to pg
-- requires: sciveyor-rails
-- requires: appschema

BEGIN;

ALTER TABLE sciveyor.users
  DROP COLUMN sign_in_count;
ALTER TABLE sciveyor.users
  DROP COLUMN current_sign_in_at;
ALTER TABLE sciveyor.users
  DROP COLUMN last_sign_in_at;
ALTER TABLE sciveyor.users
  DROP COLUMN current_sign_in_ip;
ALTER TABLE sciveyor.users
  DROP COLUMN last_sign_in_ip;

COMMIT;
