-- Revert sciveyor:tasks_attempts from pg

BEGIN;

ALTER TABLE sciveyor.tasks
  DROP COLUMN attempts;

COMMIT;
