-- Deploy sciveyor:tasks_attempts to pg
-- requires: appschema
-- requires: sciveyor-rails

BEGIN;

ALTER TABLE sciveyor.tasks
  ADD COLUMN attempts BIGINT NOT NULL DEFAULT 0;

COMMIT;
