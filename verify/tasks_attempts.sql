-- Verify sciveyor:tasks_attempts on pg

BEGIN;

SELECT attempts FROM sciveyor.tasks WHERE FALSE;

ROLLBACK;
